/***************************************************************************
 *      Mechanized Assault and Exploration Reloaded Projectfile            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "game/data/report/special/savedreportplayerleft.h"
#include "netmessage.h"
#include "game/data/player/player.h"

//------------------------------------------------------------------------------
cSavedReportPlayerLeft::cSavedReportPlayerLeft (const cPlayer& player) :
	playerName (player.getName())
{}

//------------------------------------------------------------------------------
cSavedReportPlayerLeft::cSavedReportPlayerLeft (cNetMessage& message)
{
	playerName = message.popString();
}

//------------------------------------------------------------------------------
cSavedReportPlayerLeft::cSavedReportPlayerLeft (const tinyxml2::XMLElement& element)
{
	playerName = element.Attribute ("playerName");
}

//------------------------------------------------------------------------------
void cSavedReportPlayerLeft::pushInto (cNetMessage& message) const
{
	message.pushString (playerName);

	cSavedReport::pushInto (message);
}

//------------------------------------------------------------------------------
void cSavedReportPlayerLeft::pushInto (tinyxml2::XMLElement& element) const
{
	element.SetAttribute ("playerName", playerName.c_str());

	cSavedReport::pushInto (element);
}

//------------------------------------------------------------------------------
eSavedReportType cSavedReportPlayerLeft::getType() const
{
	return eSavedReportType::PlayerLeft;
}

//------------------------------------------------------------------------------
std::string cSavedReportPlayerLeft::getMessage() const
{
	return lngPack.i18n ("Text~Multiplayer~Player_Left", playerName);
}

//------------------------------------------------------------------------------
bool cSavedReportPlayerLeft::isAlert() const
{
	return false;
}
