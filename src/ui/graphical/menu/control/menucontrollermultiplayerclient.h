/***************************************************************************
 *      Mechanized Assault and Exploration Reloaded Projectfile            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ui_graphical_menu_control_menucontrollermultiplayerclientH
#define ui_graphical_menu_control_menucontrollermultiplayerclientH

#include <memory>

#include "utility/signal/signalconnectionmanager.h"
#include "network.h"
#include "utility/runnable.h"
#include "utility/thread/concurrentqueue.h"

class cApplication;
class cWindowNetworkLobbyClient;
class cWindowLandingPositionSelection;
class cNetworkClientGameNew;
class cNetMessage;
class cMapReceiver;
class cPlayerLandingStatus;

class cTCP;

class cMenuControllerMultiplayerClient : public INetMessageReceiver, public cRunnable, public std::enable_shared_from_this<cMenuControllerMultiplayerClient>
{
public:
	cMenuControllerMultiplayerClient (cApplication& application);
	~cMenuControllerMultiplayerClient();

	void start();

	virtual void pushEvent (std::unique_ptr<cNetMessage> message) MAXR_OVERRIDE_FUNCTION;
	virtual std::unique_ptr<cNetMessage> popEvent() MAXR_OVERRIDE_FUNCTION;

	virtual void run() MAXR_OVERRIDE_FUNCTION;
private:
	cSignalConnectionManager signalConnectionManager;

	cConcurrentQueue<std::unique_ptr<cNetMessage>> messageQueue;

	std::shared_ptr<cTCP> network;

	cApplication& application;

	std::shared_ptr<cWindowNetworkLobbyClient> windowNetworkLobby;

	std::shared_ptr<cWindowLandingPositionSelection> windowLandingPositionSelection;

	std::shared_ptr<cNetworkClientGameNew> newGame;

	std::unique_ptr<cMapReceiver> mapReceiver;

	std::vector<std::unique_ptr<cPlayerLandingStatus>> playersLandingStatus;

	std::string triedLoadMapName;
	std::string lastRequestedMapName;

	void reset();

	void handleWantLocalPlayerReadyChange();
	void handleChatMessageTriggered();
	void handleLocalPlayerAttributesChanged();

	void connect();

	void startSavedGame();

	void startGamePreparation();

	void startClanSelection(bool isFirstWindowOnGamePreparation);
	void startLandingUnitSelection(bool isFirstWindowOnGamePreparation);
	void startLandingPositionSelection();
	void startNewGame();
	void checkReallyWantsToQuit();

	void handleNetMessage (cNetMessage& message);

	void handleNetMessage_TCP_CLOSE (cNetMessage& message);
	void handleNetMessage_MU_MSG_CHAT (cNetMessage& message);
	void handleNetMessage_MU_MSG_REQ_IDENTIFIKATION (cNetMessage& message);
	void handleNetMessage_MU_MSG_PLAYER_NUMBER (cNetMessage& message);
	void handleNetMessage_MU_MSG_PLAYERLIST (cNetMessage& message);
	void handleNetMessage_MU_MSG_OPTINS (cNetMessage& message);
	void handleNetMessage_MU_MSG_GO (cNetMessage& message);
	void handleNetMessage_MU_MSG_LANDING_STATE (cNetMessage& message);
	void handleNetMessage_MU_MSG_ALL_LANDED (cNetMessage& message);
	void handleNetMessage_GAME_EV_REQ_RECON_IDENT (cNetMessage& message);
	void handleNetMessage_GAME_EV_RECONNECT_ANSWER (cNetMessage& message);
	void handleNetMessage_MU_MSG_IN_LANDING_POSITION_SELECTION_STATUS (cNetMessage& message);
	void handleNetMessage_MU_MSG_PLAYER_HAS_SELECTED_LANDING_POSITION (cNetMessage& message);
	void handleNetMessage_MU_MSG_PLAYER_HAS_ABORTED_GAME_PREPARATION (cNetMessage& message);

	void initMapDownload (cNetMessage& message);
	void receiveMapData (cNetMessage& message);
	void canceledMapDownload (cNetMessage& message);
	void finishedMapDownload (cNetMessage& message);

	void saveOptions();
};

#endif // ui_graphical_menu_control_menucontrollermultiplayerclientH
